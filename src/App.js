import React from 'react';
import './App.css';
import UserForm from './components/UserForm';
// import CounterOne from './components/CounterOne';
// import CounterTwo from './components/CounterTwo';
// import DocTitleOne from './components/DocTitleOne';
// import DocTitleTwo from './components/DocTitleTwo';
// import ClassTime from './components/ClassTime';
// import HookTimer from './components/HookTimer';
// import FocusInput from './components/FocusInput';
// import Counter from './components/Counter';
// import ParentComponent from './components/ParentComponent';
// import DataFetchingOne from './components/DataFetchingOne';
// import DataFetchingTwo from './components/DataFetchingTwo';

function App() {
  return (
    <div className="App">
      <UserForm />
      {/* <CounterOne />
      <CounterTwo /> */}
      {/* <DocTitleOne />
      <DocTitleTwo /> */}
      {/* <ClassTime /> 
      <HookTimer /> */}
      {/* <FocusInput /> */}
      {/* <Counter /> */}
      {/* <ParentComponent /> */}
      {/* <DataFetchingTwo /> */}
      {/* <DataFetchingOne /> */}
    </div>
  );
}

export default App;
